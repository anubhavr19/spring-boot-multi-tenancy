package com.example.model;

public interface TenantSupport {
  String getOrgnizationId();

  void setOrgnizationId(String orgnizationId);
}
